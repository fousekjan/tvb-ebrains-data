from setuptools import find_packages, setup
  
setup(
    name='tvb-ebrains-data',
    packages=find_packages(),
    version='0.2.2',
    install_requires=[
        'tvb-library',
        'pooch',
    ]
)
