from .data import HCPDataset
from .data import HCPDomhof
from .data import Brains1000Dataset


from pkg_resources import get_distribution
__version__ = get_distribution('tvb_ebrains_data').version
