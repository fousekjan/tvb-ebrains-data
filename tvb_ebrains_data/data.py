import logging
import os
import io
import requests
import numpy as np
import pooch
from glob import glob
from tvb.simulator.lab import *

logger = logging.getLogger(__name__)

try:
    from clb_nb_utils import oauth
    token = oauth.get_token()
except ImportError as err:
    logger.warning('Token required outside collaboratory environment. Set EBRAINS_TOKEN in the environment or provide directly.')


class DataProxyConnectivityDataset:
    def __init__(self, bucket_name, token=None):
        self.BUCKET_NAME = bucket_name # dataset specific
        self.DATA_PROXY_ENDPOINT = "https://data-proxy.ebrains.eu/api/v1"  # refactor to base url

        if token is None:
            token = self._get_token()

        self.AUTHORIZATION_HEADERS = {
            "Authorization": f"Bearer {token}"
        }

    def _get_token(self):
        token = os.getenv('EBRAINS_TOKEN')
        if token is None: # env variable not set
            try:
                from clb_nb_utils import oauth
                token = oauth.get_token()
            except ImportError as err:
                raise Exception('Token required outside collaboratory environment. Set EBRAINS_TOKEN in the environment or provide directly.') from err
        return token

    def open_file(self, path):
        temporary_url = requests.get(
            f"{self.DATA_PROXY_ENDPOINT}/datasets/{self.BUCKET_NAME}/{path}", 
            headers=self.AUTHORIZATION_HEADERS,
            params={'redirect':False}
        )
        if not temporary_url.ok:
            raise Exception(f'Failed request: {temporary_url.reason}')
        dl_url = temporary_url.json()["url"]
        dl = requests.get(dl_url, stream=True)
        if dl.status_code == 200:
            return io.BytesIO(dl.content)
        else:
            raise Exception(f'Temporary URL request failed: {dl.status_code}')

    def _get_all(self,query, limit=50):
        query = f"{query}&limit={limit}"
        all_objects = []
        stats = requests.get( query, headers=self.AUTHORIZATION_HEADERS)
        if not stats.ok:
            raise Exception(f'Failed request: {stats.reason}')
        while len(stats.json()['objects'])>0:
            all_objects.extend(stats.json()['objects'])
            if 'name' in all_objects[-1]:
                marker = all_objects[-1]['name']
            else:
                marker = all_objects[-1]['subdir']
            stats = requests.get(
                f'{query}&marker={marker}',
                headers=self.AUTHORIZATION_HEADERS
            )
        return all_objects


    def list_subjects(self):
        raise NotImplementedError()

    def load_sc(self, subject):
        raise NotImplementedError()
    
    def have_access(self):
        response = requests.get(
            url = f'{self.DATA_PROXY_ENDPOINT}/datasets/{self.BUCKET_NAME}/stats',
            headers = self.AUTHORIZATION_HEADERS
        )
        if response.ok:
            return True
        elif response.status_code == 401:
            return False
        else:
            raise Exception(f'Failed request: {response.reason}')

    def request_access(self):
        response = requests.post(
            url = f'{self.DATA_PROXY_ENDPOINT}/datasets/{self.BUCKET_NAME}',
            headers = self.AUTHORIZATION_HEADERS
        )
        if not response.ok:
            raise Exception(f'Failed request: {response.reason}')
        else:
            print('Request for access submitted, check mail.')


class HCPDataset(DataProxyConnectivityDataset):
    """
    Domhof, J. W. M., Jung, K., Eickhoff, S. B., & Popovych, O. V. (2021).
    Parcellation-based structural and resting-state functional brain
    connectomes of a healthy cohort [Data set]. EBRAINS.

    https://doi.org/10.25493%2F81EV-ZVT
    """

    def __init__(self, version='v1.0', token=None):
        self.version = version
        super().__init__(bucket_name="d61fc54a-7cc1-4126-93c0-9b6d97775421", token=token)


    def list_parcellations(self): # TODO cache this ?
        parcs = self._get_all(
            f"{self.DATA_PROXY_ENDPOINT}/datasets/{self.BUCKET_NAME}?delimiter=/&prefix={self.version}/"
        )
        return [ o['subdir'][len(f'{self.version}/'):-1] for o in parcs if 'subdir' in o]

    def list_subjects(self, parcellation): # TODO cache this ?
        prefix = f'{self.version}/{parcellation}/1StructuralConnectivity'
        subjs = self._get_all(
            f"{self.DATA_PROXY_ENDPOINT}/datasets/{self.BUCKET_NAME}?delimiter=/&prefix={prefix}/",
        )
        return [ o['subdir'][len(f'{prefix}/'):-1] for o in subjs if 'subdir' in o]

    def load_sc(self, subject, parcellation):
        W = np.loadtxt(self.open_file(f'{self.version}/{parcellation}/1StructuralConnectivity/{subject}/Counts.csv'))
        D = np.loadtxt(self.open_file(f'{self.version}/{parcellation}/1StructuralConnectivity/{subject}/Lengths.csv'))
        return W, D


class HCPDomhof():
    """
    - https://doi.org/10.25493/NVS8-XS5
    - https://doi.org/10.25493/F9DP-WCQ

    Available parcellations: 
       031-MIST                      038-CraddockSCorr2Level
       048-HarvardOxfordMaxProbThr0  056-CraddockSCorr2Level
       056-MIST                      070-DesikanKilliany 
       079-Shen2013                  086-EconomoKoskinas 
       092-AALV2                     096-HarvardOxfordMaxProbThr0
       100-Schaefer17Networks        103-MIST 
       108-CraddockSCorr2Level       150-Destrieux 
       156-Shen2013                  160-CraddockSCorr2Level
       167-MIST                      200-Schaefer17Networks 
       210-Brainnetome               294-Julich-Brain

    The tsv files for the time-series are double space separated and have a trailing space ??
    ```
    find . -name '*.tsv' -exec  sed -i 's/[ \t]*$//'  {} \;
    find . -name '*.tsv' -exec  sed -i 's/  /,/g'  {} \;
    ```
    """
    parcellations = {
            '031-MIST'                       : (None, None), # connectivity, bold
            '038-CraddockSCorr2Level'        : (None, None),
            '048-HarvardOxfordMaxProbThr0'   : (None, None),
            '056-CraddockSCorr2Level'        : (None, None),
            '056-MIST'                       : (None, None),
            '070-DesikanKilliany'            : ('419c59ab4e01059265cad42e5e68d7e58b0381bc27fe47ad4d4218358ba76280',
                                                '26812c39d3963924d3c8fdf65dc3e08b6be13e7d6a5f91e7a13734714958f5fc'),
            '079-Shen2013'                   : (None, None),
            '086-EconomoKoskinas'            : (None, None),
            '092-AALV2'                      : (None, None),
            '096-HarvardOxfordMaxProbThr0'   : (None, None),
            '100-Schaefer17Networks'         : ('de583e85dd4aa1c0521d61d73f290ad6da3ce4cd5b3538c62b7630f03e438157', 
                                                '650d5bf9a103299c6505a129051b5bc54a413a9d5ddffb574a4d165c12b1457f'),
            '103-MIST'                       : (None, None),
            '108-CraddockSCorr2Level'        : (None, None),
            '150-Destrieux'                  : (None, None),
            '156-Shen2013'                   : (None, None),
            '160-CraddockSCorr2Level'        : (None, None),
            '167-MIST'                       : (None, None),
            '200-Schaefer17Networks'         : ('5086f4b3405acff84ffe132cee17c67a90000a3fae98da50d4e14fb55d7f5d57', 
                                                'md5:1f25b912465fe651f5338a7f106f5fe0'),
            '210-Brainnetome'                : (None, None),
            '294-Julich-Brain'               : (None, None),
    }

    def __init__(self, data_root, parcellation='200-Schaefer17Networks'):
        self.data_root = data_root
        assert parcellation in self.parcellations, 'Incorrect parcellation name.'
        _ = pooch.retrieve(
            url=f'https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000067_Atlas_based_HCP_BOLD_pub/v1.0/{parcellation}.zip',
            known_hash=self.parcellations[parcellation][1],
            path=os.path.join(self.data_root, 'bold'),
            processor=pooch.Unzip(extract_dir='.')
        )
        _ = pooch.retrieve(
            url=f'https://object.cscs.ch/v1/AUTH_227176556f3c4bb38df9feea4b91200c/hbp-d000059_Atlas_based_HCP_connectomes_v1.1_pub/{parcellation}.zip',
            known_hash=self.parcellations[parcellation][0],
            path=os.path.join(self.data_root, 'connectivity'),
            processor=pooch.Unzip(extract_dir='.')
        )
        self.subjects = [os.path.basename(p) for p in glob(os.path.join(self.data_root, 'bold', parcellation, '*[0-9]')) ]
        self.parcellation = parcellation

    def load_sc(self, subj):
        """
        Load structural connectivity.

        Parameters
        ----------
            subj : str
                Subject id from `self.subjects`.

        Returns
        -------
            weights: ndarray
                Weights matrix [N,N].
            tract_length: ndarray
                Tract length matrix [N,N].
        """
        weights = np.loadtxt(
            os.path.join(self.data_root, 'connectivity', self.parcellation, '1StructuralConnectivity', subj, 'Counts.csv'),
            delimiter=' ',
        )
        tract_lengths = np.loadtxt(
            os.path.join(self.data_root, 'connectivity', self.parcellation, '1StructuralConnectivity', subj, 'Lengths.csv'),
            delimiter=' ',
        )
        return weights, tract_lengths

    def load_bold(self, subj):
        bold = np.loadtxt(
            os.path.join(self.data_root, 'bold', self.parcellation, subj, 'rfMRI_REST1_LR_BOLD.tsv'),
        )
        return bold





class Brains1000Dataset(DataProxyConnectivityDataset):
    """
    Caspers, S. et al (2021).
    1000BRAINS study, connectivity data. 

    v1.0: https://doi.org/10.25493/61QA-KP8
    v1.1: https://doi.org/10.25493/6640-3XH
    """
    def __init__(self, version='v1.1', token=None):
        self.version = version
        if version=='v1.1':
            super().__init__(bucket_name="83407c06-b494-4307-861e-d06a5aecdf8a", token=token)
        elif version=='v1.0':
            super().__init__(bucket_name="e428cb6b-0110-4205-94ac-533ca5de6bb5", token=token)

    def list_subjects(self): # TODO cache this ?
        prefix = f'{self.version}/Julich-Brain/Streamlines'
        subjs = self._get_all(
            f"{self.DATA_PROXY_ENDPOINT}/datasets/{self.BUCKET_NAME}?delimiter=/&prefix={prefix}/",
        )
        return [ o['name'][len(f'{prefix}/'):-4] for o in subjs if 'name' in o]

    def load_sc(self, subject, log10=False):
        if log10:
            fname = f'{subject}_SC_7NW100parc_log10.txt'
        else:
            fname = f'{subject}_SC_7NW100parc.txt'
        W = np.loadtxt(self.open_file(f'{self.version}/Schaefer/Matrices/{fname}'))
        return W

    def get_connectivity(self, subject, scaling_factor=124538.470647693):
        SC = self.load_sc(subject)
        SC = SC / scaling_factor
        conn = connectivity.Connectivity(
                weights = SC,
                tract_lengths=np.ones_like(SC),
                centres = np.zeros(np.shape(SC)[0]),
                speed = np.r_[np.Inf]
        )
        conn.compute_region_labels()
        logger.warning("Placeholder region names!")
        return conn

